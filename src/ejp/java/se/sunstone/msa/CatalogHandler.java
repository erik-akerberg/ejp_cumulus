package se.sunstone.msa;

import com.canto.cumulus.*;

import java.util.Set;

public class CatalogHandler {
    Catalog cumCatalog;

    GUID recordNameField;
    GUID idField;
    GUID ingestStatusField;
    GUID archivingStatusField;
    GUID lrArchivingStatusField = getFieldGUID(Config.LR_ARCHIVING_STATUS_FIELD_NAME);
    GUID hrArchivingStatusField = getFieldGUID(Config.HR_ARCHIVING_STATUS_FIELD_NAME);
    GUID logField;
    GUID assetRefField;
    GUID assetNameField;
    GUID highResNameField;
    GUID abortField;
    GUID deleteField;
    GUID archiveIdField;
    GUID highResArchiveIdField;

    public CatalogHandler(Catalog catalog) {
        cumCatalog = catalog;

        recordNameField = getFieldGUID(Config.RECORD_NAME_FIELD_NAME);
        idField = getFieldGUID(Config.ID_FIELD_NAME);
        ingestStatusField = getFieldGUID(Config.INGEST_STATUS_FIELD_NAME);
        archivingStatusField = getFieldGUID(Config.ARCHIVING_STATUS_FIELD_NAME);
        logField = getFieldGUID(Config.LOG_FIELD_NAME);
        assetRefField = getFieldGUID(Config.ASSET_REF_FIELD_NAME);
        assetNameField = getFieldGUID(Config.ASSET_NAME_FIELD_NAME);
        highResNameField = getFieldGUID(Config.HIGH_RES_NAME_FIELD_NAME);
        abortField = getFieldGUID(Config.ABORT_FIELD_NAME);
        deleteField = getFieldGUID(Config.DELETE_FIELD_NAME);
        archiveIdField = getFieldGUID(Config.ARCHIVE_ID_FIELD_NAME);
        highResArchiveIdField = getFieldGUID(Config.HIGH_RES_ARCHIVE_ID_FIELD_NAME);
    }

    private GUID getFieldGUID(String fieldName) {
        Layout layout = cumCatalog.getLayout(Cumulus.TABLE_NAME_ASSET_RECORDS);
        Set<GUID> fieldUIDs = layout.getFieldUIDs();

        for (GUID guid : fieldUIDs) {
            FieldDefinition fieldDefinition = layout.getFieldDefinition(guid);
            if (fieldDefinition.getName().equals(fieldName)) {
                return guid;
            }
        }
        return null;
    }
}
