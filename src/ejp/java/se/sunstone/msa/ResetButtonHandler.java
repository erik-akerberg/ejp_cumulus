package se.sunstone.msa;

import com.canto.cumulus.ui.event.ActionHandler;

import java.awt.event.ActionEvent;
import java.util.ArrayList;

public class ResetButtonHandler implements ActionHandler {

    @Override
    public void handleAction(ActionEvent actionEvent) {
        ArrayList<ItemHandler> selectedItems = CumUtils.getSelectedItems();

        if (selectedItems.size() == 0) {
            CumUtils.show("Choose records to reset.");
            return;
        }

        for (ItemHandler selectedItem : selectedItems) {

            if (selectedItem.getIngestStatus().equals("Ingesting") &&
                    selectedItem.getArchivingStatus().equals("Waiting")) {
                selectedItem.setIngestStatus("Ready");
                selectedItem.log("Ingest reset.");
                selectedItem.save();
                continue;
            }

            String[] lrResetInfo = getResetInfo(selectedItem.getLrArchivingStatus());
            String[] hrResetInfo = getResetInfo(selectedItem.getHrArchivingStatus());

            if (lrResetInfo[0] == null && hrResetInfo[0] == null) {
                CumUtils.show(selectedItem.getDisplayString() + ": Record not in resettable state.");
            } else if (lrResetInfo[0] != null) {
                selectedItem.setLrArchivingStatus(lrResetInfo[0]);
                selectedItem.log(lrResetInfo[1]);
            } else {
                selectedItem.setHrArchivingStatus(hrResetInfo[0]);
                selectedItem.log(hrResetInfo[1]);
            }

            selectedItem.save();
        }
    }

    public String[] getResetInfo(String status) {
        switch (status) {
            case "Encryption_failed":
                return new String[]{"Encryption_reset", "Encryption reset."};
            case "Submitted":
            case "Packaging_failed":
                return new String[]{"Packaging_reset", "Packaging reset."};
            case "Submit_failed":
                return new String[]{"Submit_reset", "Submit reset."};
            default:
                return new String[]{null, null};
        }
    }
}
