package se.sunstone.msa;

import com.canto.cumulus.ui.event.ActionHandler;

import java.awt.event.ActionEvent;
import java.util.ArrayList;

public class AbortButtonHandler implements ActionHandler {
    @Override
    public void handleAction(ActionEvent actionEvent) {

        ArrayList<ItemHandler> items = CumUtils.getSelectedItems();

        if (items.size() == 0) {
            CumUtils.show("Select records to abort.");
            return;
        }

        for (ItemHandler selectedItem : items) {
            selectedItem.setAborted(true);
            selectedItem.log("Marked for abort.");
            selectedItem.save();
        }
    }
}
