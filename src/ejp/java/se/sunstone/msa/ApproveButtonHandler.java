package se.sunstone.msa;

import com.canto.cumulus.ui.event.ActionHandler;

import java.awt.event.ActionEvent;
import java.util.ArrayList;

public class ApproveButtonHandler implements ActionHandler {
    @Override
    public void handleAction(ActionEvent actionEvent) {
        ArrayList<ItemHandler> items = CumUtils.getSelectedItems();

        if (items.size() == 0) {
            CumUtils.show("Select records to approve.");
            return;
        }

        for (ItemHandler selectedItem : items) {

            String ingestStatus = selectedItem.getIngestStatus();
            String archivingStatus = selectedItem.getArchivingStatus();

            switch (ingestStatus+ "," + archivingStatus) {
                case "Ingested,Waiting":
                case "N/A,Ready":
                    break;
                default:
                    CumUtils.show(selectedItem.getDisplayString() + ": Not awaiting approval.");
                    continue;
            }

            if (selectedItem.getAssetName() == null) {
                CumUtils.show(selectedItem.getDisplayString() + ": No asset to approve.");
                continue;
            }

            selectedItem.setArchivingStatus("Approved");

            selectedItem.log("Approved for archiving.");

            selectedItem.save();
        }
    }
}
