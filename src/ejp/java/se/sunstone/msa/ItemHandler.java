package se.sunstone.msa;

import com.canto.cumulus.Item;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ItemHandler {

    final private Item cumItem;
    CatalogHandler catalog;

    public ItemHandler(Item cumItm) {
        cumItem = cumItm;
        catalog = CumUtils.catalogs.get(CumUtils.catalogIds.indexOf(cumItem.getCatalogID()));

    }

    public String getId() {
        return cumItem.getStringValue(catalog.idField);
    }

    public String getAssetName() {
        if (!cumItem.hasValue(catalog.assetNameField)) {
            return null;
        }
        return cumItem.getStringValue(catalog.assetNameField);
    }

    public String getIngestStatus() {
        return cumItem.getStringValue(catalog.ingestStatusField);
    }

    public String getArchivingStatus() {
        return cumItem.getStringValue(catalog.archivingStatusField);
    }

    public String getDisplayString() {
        return getId();
    }

    public String getHighResName() {
        return cumItem.getStringValue(catalog.highResNameField);
    }

    public String getLrArchivingStatus() {
        return cumItem.getStringValue(catalog.lrArchivingStatusField);
    }

    public String getHrArchivingStatus() {
        return cumItem.getStringValue(catalog.hrArchivingStatusField);
    }

    public void setLrArchivingStatus(String status) {
        cumItem.setStringValue(catalog.lrArchivingStatusField, status);
    }

    public void setHrArchivingStatus(String status) {
        cumItem.setStringValue(catalog.hrArchivingStatusField, status);
    }

    public void setAborted(boolean aborted) {
        cumItem.setBooleanValue(catalog.abortField, aborted);
    }

    public void setDelete(boolean delete) {
        cumItem.setBooleanValue(catalog.deleteField, delete);
    }

    public void setIngestStatus(String status) {
        cumItem.setStringValue(catalog.ingestStatusField, status);
    }

    public void setArchivingStatus(String status) {
        cumItem.setStringValue(catalog.archivingStatusField, status);
    }

    public void setAssetName(String name) {
        cumItem.setStringValue(catalog.assetNameField, name);
    }

    public void setHighResName(String name) {
        cumItem.setStringValue(catalog.highResNameField, name);
    }

    public void log(String entry) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("ddMMyy hh:mm:ss: ");
        LocalDateTime now = LocalDateTime.now();

        String oldLogs = "";

        try {
            oldLogs = cumItem.getStringValue(catalog.logField);
        } catch (Exception e) {
            //Do nothing
        }

        cumItem.setStringValue(catalog.logField, oldLogs + dtf.format(now) + entry + "\n");
        Logger.info(getId() + " : " + entry);
    }

    public void save() {
        cumItem.save();
    }
}
