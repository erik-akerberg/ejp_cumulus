package se.sunstone.msa;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

public class Logger {

    static String logFileDirectory;
    static Path logFile;

    public static void info(String line) {
        log("INFO", line);
    }

    public static void warn(String line) {
        log("WARN", line);
    }

    public static void error(String line, Throwable t) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        t.printStackTrace(pw);
        System.out.println(sw.toString());
        log("ERROR", line + "\n" + sw.toString());
    }

    private static void log(String level, String line) {
        try {
            init(logFileDirectory);
            String message = ("[" + level + "][" + DateTimeFormatter.ISO_DATE_TIME.format(OffsetDateTime.now()) + "] - " + line + "\n");
            Files.write(logFile, message.getBytes(Charset.forName("UTF-8")), StandardOpenOption.APPEND);
        }
        catch (IOException e) {
            //Well... we cant log this
        }
    }

    public static void init(String logFileDir) {

        logFileDirectory = logFileDir;

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("ddMMyyyy");
        LocalDateTime now = LocalDateTime.now();

        String logFileName = dtf.format(now) + ".log";

        logFile = Paths.get(logFileDir + logFileName);

        try {
            if (Files.exists(logFile)) {
                //Do nothing
            }
            else {
                Files.createFile(logFile);
            }
        } catch (IOException e) {
            //Well... we cant log this
        }
    }
}
