package se.sunstone.msa;

import com.canto.cumulus.ui.event.ActionHandler;

import java.awt.event.ActionEvent;
import java.util.ArrayList;

public class ConfirmSubmitButtonHandler implements ActionHandler {
    @Override
    public void handleAction(ActionEvent actionEvent) {
        ArrayList<ItemHandler> items = CumUtils.getSelectedItems();

        if (items.size() == 0) {
            CumUtils.show("Select records to confirm.");
            return;
        }

        for (ItemHandler selectedItem : items) {

            String archivingStatus = selectedItem.getArchivingStatus();

            if (!archivingStatus.equals("Submitted")) {
                CumUtils.show(selectedItem.getDisplayString() + ": Not awaiting accepting.");
                continue;
            }

            selectedItem.setArchivingStatus("Accepted");

            selectedItem.log("Submission accepted.");

            selectedItem.save();
        }
    }
}
