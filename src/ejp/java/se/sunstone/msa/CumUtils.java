package se.sunstone.msa;

import com.canto.cumulus.*;
import com.canto.cumulus.ui.Application;
import com.canto.cumulus.ui.CollectionWindow;
import com.canto.cumulus.ui.MultiItemPane;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Set;

public class CumUtils {

    static ArrayList<CatalogHandler> catalogs = new ArrayList<>();
    static ArrayList<Integer> catalogIds = new ArrayList<>();

    public static void loadCatalogs() {
        CollectionWindow collectionWindow = Application.getInstance().getCurrentCollectionWindow();
        MultiItemCollection multiItemCollection = collectionWindow.getMainRecordPane().getMultiItemCollection();

        Set<Integer> allCatalogIds = multiItemCollection.getServer().getCatalogIDs(true, false);

        for (int catalogId : allCatalogIds) {
            try {
                Catalog catalog = multiItemCollection.getServer().openCatalog(catalogId);

                if (Config.CUMULUS_CATALOG_NAMES.contains(catalog.getName())) {
                    catalogs.add(new CatalogHandler(catalog));
                    catalogIds.add(catalogId);
                }
            } catch (Exception e) {
                show("Error opening catalog " + catalogId);
            }

        }
    }

    public static ArrayList<ItemHandler> getSelectedItems() {
        if (catalogs.size() == 0) {
            loadCatalogs();
        }

        ArrayList<ItemHandler> selectedItems = new ArrayList<>();

        getMainPane().forEach(item -> {
            if (item instanceof RecordItem) {
                if (catalogIds.contains(item.getCatalogID())) {
                    selectedItems.add(new ItemHandler(item));
                } else {
                    CumUtils.show(item.getDisplayString() + " not in an allowed catalog.");
                }
            }
        });

        return selectedItems;
    }

    private static MultiItemPane getMainPane() {
        return Application.getInstance().getCurrentCollectionWindow().getMainRecordPane();
    }

    public static void show(String message) {
        JOptionPane.showMessageDialog(null, message);
    }

    public static int prompt(String message) {
        return JOptionPane.showConfirmDialog(null, message);
    }
}
