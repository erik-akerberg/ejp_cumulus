package se.sunstone.msa;

import com.canto.cumulus.ui.event.ActionHandler;

import java.awt.event.ActionEvent;
import java.util.ArrayList;

public class MakeIngestableButtonHandler implements ActionHandler {
    @Override
    public void handleAction(ActionEvent actionEvent) {
        ArrayList<ItemHandler> items = CumUtils.getSelectedItems();

        if (items.size() == 0) {
            CumUtils.show("Select records to make ingestable.");
            return;
        }

        for (ItemHandler selectedItem : items) {

            String ingestStatus = selectedItem.getIngestStatus();
            String archivingStatus = selectedItem.getArchivingStatus();


            if (ingestStatus.equals("N/A") && archivingStatus.equals("Ready")) {
                if (selectedItem.getAssetName() != null) {
                    CumUtils.show(selectedItem.getDisplayString() + ": Item already has asset.");
                    continue;
                }

                selectedItem.setIngestStatus("Ready");
                selectedItem.setArchivingStatus("Waiting");
                selectedItem.log("Made ingestable");
                selectedItem.save();
            } else {
                CumUtils.show(selectedItem.getDisplayString() + ": Item not in correct state.");
            }
        }
    }
}
