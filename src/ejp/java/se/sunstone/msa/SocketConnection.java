package se.sunstone.msa;

import java.io.*;

import org.apache.commons.net.telnet.TelnetClient;

public class SocketConnection {

    private TelnetClient telnet = new TelnetClient();
    private OutputStream out;
    private BufferedReader in;

    String endline = "\r\n";

    public SocketConnection(String host, int port) throws IOException {

        telnet.connect(host, port);

        out = telnet.getOutputStream();
        in = new BufferedReader(new InputStreamReader(telnet.getInputStream()));

    }

    private void sendCommand(String command) {
        try {
            out.write(command.getBytes());
            out.flush();
        } catch (Exception e) {
            CumUtils.show("Error when sending message.");
        }
    }

    public boolean isRunning(String encoder) {
        String available_command = "EncStatus " + encoder + endline;
        sendCommand(available_command);
        String resp = getResponse();

        return resp.contains("Runned");
    }

    public boolean loadProject(String projectPath) {
        String filename_command = "load " + projectPath + endline;
        sendCommand(filename_command);
        String response = getResponse();
        if (checkResponse(response)) {
            if (response.contains("close")) {
                getResponse();
            }
            return true;
        }
        return false;

    }

    public boolean setFilename(String encoder, String filename) {
        String filename_command = "setfile " + encoder + " " + filename + endline;
        sendCommand(filename_command);
        return checkResponse(getResponse());
    }

    public boolean startIngest(String encoder, int profileNumber) {
        String start_command = "Start " + encoder + " " + profileNumber + endline;
        sendCommand(start_command);
        return checkResponse(getResponse());
    }

    public String getResponse() {

        String response;

        try {
            while (true) {
                response = in.readLine();

                if (response.length() > 1) {
                    return response;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    public boolean checkResponse(String response) {
        return !response.contains("FAILED");
    }

    public void close() throws IOException {
        telnet.disconnect();
    }

}