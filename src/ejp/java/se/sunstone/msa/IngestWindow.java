package se.sunstone.msa;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class IngestWindow extends JFrame implements ActionListener {

    JComboBox<String> encoderList;
    JComboBox<String> profileList;

    ItemHandler itemToIngest;

    public IngestWindow(ItemHandler item) {
        itemToIngest = item;

        setTitle("Ingest");

        setLocation(200,200);

        String[] encoderStrings = Config.METUS_ENCODERS;
        String[] profileStrings = Config.METUS_PROFILES;

        encoderList = new JComboBox<String>(encoderStrings);
        JLabel encoderLabel = new JLabel("Choose encoder:");

        profileList = new JComboBox<String>(profileStrings);
        JLabel profileLabel = new JLabel("Choose profile:");

        JButton okButton = new JButton("OK");
        okButton.addActionListener(this);

        encoderList.setBounds(130,20, 110,30);
        encoderLabel.setBounds(20, 15, 110, 50);

        profileList.setBounds(130,60, 110,30);
        profileLabel.setBounds(20, 55, 110, 50);

        okButton.setBounds(80, 120, 80, 30);

        add(encoderList);
        add(encoderLabel);

        add(profileList);
        add(profileLabel);

        add(okButton);

        setSize(280,200);
        setLayout(null);
        setVisible(true);
    }

    //Ok button pressed
    public void actionPerformed(ActionEvent e) {
        String encoder = (String) encoderList.getSelectedItem();
        String profile = (String) profileList.getSelectedItem();

        setVisible(false);
        dispose();

        MetusTalker metusTalker = new MetusTalker(itemToIngest, encoder, profile);
    }
}
