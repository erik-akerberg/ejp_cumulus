package se.sunstone.msa;

import com.canto.cumulus.ejp.AbstractGenericEJP;
import com.canto.cumulus.ejp.EJPContext;
import com.canto.cumulus.modules.ejpregistries.EJPPreferencePanel;
import com.canto.cumulus.ui.*;
import com.canto.cumulus.ui.event.ActionHandler;

import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;

public class EJP extends AbstractGenericEJP implements ActionHandler {

    @Override
    public void initialize(EJPContext context) {
        super.initialize(context);

        String configFilePath = new File(context.getLocation().getFile())
                .getParentFile()
                .getAbsolutePath()
                .replaceAll("%20", " ")
                + "\\lib\\config.properties";

        String logFileDir = new File(context.getLocation().getFile())
                .getParentFile()
                .getAbsolutePath()
                .replaceAll("%20", " ")
                + "\\lib\\log\\";

        try {
            Config.loadProperties(configFilePath);
        } catch (Exception e) {
            //Handle this somehow
        }
        Logger.init(logFileDir);
        createMenu();
    }

    public void createMenu() {
        MenuBar menuBar = Application.getInstance().getMenuBar();
        Menu menu = new Menu("MSA");

        menuBar.addMenu(menuBar.countMenus() - 1, menu);

        MenuItem ingestButton = new MenuItem("Ingest");
        ingestButton.setActionHandler(this);

        MenuItem approveButton = new MenuItem("Approve for archiving");
        approveButton.setActionHandler(new ApproveButtonHandler());

        MenuItem makeIngestableButton = new MenuItem("Make ingestable");
        makeIngestableButton.setActionHandler(new MakeIngestableButtonHandler());

        MenuItem resetButton = new MenuItem("Reset");
        resetButton.setActionHandler(new ResetButtonHandler());

        MenuItem abortButton = new MenuItem("Abort");
        abortButton.setActionHandler(new AbortButtonHandler());

        MenuItem DeleteButton = new MenuItem("Delete");
        DeleteButton.setActionHandler(new DeleteButtonHandler());

        MenuItem ConfirmSubmitButton = new MenuItem("Confirm submit");
        ConfirmSubmitButton.setActionHandler(new ConfirmSubmitButtonHandler());

        menu.addMenuItem(ingestButton);
        menu.addMenuItem(approveButton);
        menu.addMenuItem(ConfirmSubmitButton);
        menu.addMenuItem(resetButton);
        menu.addMenuItem(makeIngestableButton);
        menu.addMenuItem(abortButton);
        menu.addMenuItem(DeleteButton);
    }

    //Ingest button pressed
    @Override
    public void handleAction(ActionEvent actionEvent) {

        ArrayList<ItemHandler> items = CumUtils.getSelectedItems();

        if (items.size() != 1) {
            CumUtils.show("Select one record to ingest.");
            return;
        }

        ItemHandler selectedItem = items.get(0);

        if (!selectedItem.getIngestStatus().equals("Ready")) {
            CumUtils.show(selectedItem.getDisplayString() + ": Not in ingest ready state.");
            return;
        }

        if (!selectedItem.getArchivingStatus().equals("Waiting")) {
            CumUtils.show(selectedItem.getDisplayString() + ": Archiving has begun.");
            return;
        }

        IngestWindow window = new IngestWindow(selectedItem);
    }

    @Override
    public String getName() {
        return "MSA EJP";
    }

    @Override
    public EJPPreferencePanel getPreferencePanel() {
        return null;
    }
}
