package se.sunstone.msa;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Properties;

public class Config {
    static String METUS_HOST;
    static int METUS_PORT;
    static String METUS_PROJECT_PATH;
    static int METUS_NUMBER_OF_ENCODERS;
    static String[] METUS_ENCODERS;
    static int METUS_NUMBER_OF_PROFILES;
    static String[] METUS_PROFILES;
    static String[] METUS_PROFILE_FILETYPES;

    static int CUMULUS_NUMBER_OF_CATALOGS;
    static ArrayList<String> CUMULUS_CATALOG_NAMES = new ArrayList<>();

    static String RECORD_NAME_FIELD_NAME;
    static String ID_FIELD_NAME;
    static String INGEST_STATUS_FIELD_NAME;
    static String ARCHIVING_STATUS_FIELD_NAME;
    static String LR_ARCHIVING_STATUS_FIELD_NAME;
    static String HR_ARCHIVING_STATUS_FIELD_NAME;
    static String LOG_FIELD_NAME;
    static String ASSET_REF_FIELD_NAME;
    static String ASSET_NAME_FIELD_NAME;
    static String HIGH_RES_NAME_FIELD_NAME;
    static String ABORT_FIELD_NAME;
    static String DELETE_FIELD_NAME;
    static String ARCHIVE_ID_FIELD_NAME;
    static String HIGH_RES_ARCHIVE_ID_FIELD_NAME;

    public static void loadProperties(String configFilePath) throws Exception {

        InputStream input = new FileInputStream(configFilePath);

        Properties properties = new Properties();
        properties.load(input);

        CUMULUS_NUMBER_OF_CATALOGS = Integer.parseInt(properties.getProperty("cumulus.catalogs"));

        for (int i = 1; i <= CUMULUS_NUMBER_OF_CATALOGS; i++) {
            CUMULUS_CATALOG_NAMES.add(properties.getProperty("cumulus.catalog." + i + ".name"));
        }

        METUS_HOST = properties.getProperty("metus.host");
        METUS_PORT = Integer.parseInt(properties.getProperty("metus.port"));
        METUS_PROJECT_PATH = properties.getProperty("metus.project.path");
        METUS_NUMBER_OF_ENCODERS = Integer.parseInt(properties.getProperty("metus.encoders"));
        METUS_ENCODERS = new String[METUS_NUMBER_OF_ENCODERS];

        for (int i = 0; i < METUS_NUMBER_OF_ENCODERS; i++) {
            METUS_ENCODERS[i] = properties.getProperty("metus.encoder." + (i+1));
        }

        METUS_NUMBER_OF_PROFILES = Integer.parseInt(properties.getProperty("metus.profiles"));
        METUS_PROFILES = new String[METUS_NUMBER_OF_PROFILES];
        METUS_PROFILE_FILETYPES = new String[METUS_NUMBER_OF_PROFILES];

        for (int i = 0; i < METUS_NUMBER_OF_PROFILES; i++) {
            METUS_PROFILES[i] = properties.getProperty("metus.profile." + (i+1));
            METUS_PROFILE_FILETYPES[i] = properties.getProperty("metus.profile." + (i+1) + ".filetype");
        }

        RECORD_NAME_FIELD_NAME = properties.getProperty("cumulus.catalog.record.name.field.name");
        ID_FIELD_NAME = properties.getProperty("cumulus.catalog.id.field.name");
        INGEST_STATUS_FIELD_NAME = properties.getProperty("cumulus.catalog.ingest.status.field.name");
        ARCHIVING_STATUS_FIELD_NAME = properties.getProperty("cumulus.catalog.archiving.status.field.name");
        LR_ARCHIVING_STATUS_FIELD_NAME = properties.getProperty("cumulus.catalog.lr.archiving.status.field.name");
        HR_ARCHIVING_STATUS_FIELD_NAME = properties.getProperty("cumulus.catalog.hr.archiving.status.field.name");
        LOG_FIELD_NAME = properties.getProperty("cumulus.catalog.log.field.name");
        ASSET_REF_FIELD_NAME = properties.getProperty("cumulus.catalog.asset.ref.field.name");
        ASSET_NAME_FIELD_NAME = properties.getProperty("cumulus.catalog.asset.name.field.name");
        HIGH_RES_NAME_FIELD_NAME = properties.getProperty("cumulus.catalog.high.res.name.field.name");
        ABORT_FIELD_NAME = properties.getProperty("cumulus.catalog.abort.field.name");
        DELETE_FIELD_NAME = properties.getProperty("cumulus.catalog.delete.field.name");
        ARCHIVE_ID_FIELD_NAME = properties.getProperty("cumulus.catalog.archive.id.field.name");
        HIGH_RES_ARCHIVE_ID_FIELD_NAME = properties.getProperty("cumulus.catalog.high.res.archive.id.field.name");
    }
}
