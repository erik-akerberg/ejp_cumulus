package se.sunstone.msa;

public class MetusTalker {

    public MetusTalker(ItemHandler item, String encoder, String profile) {

        int profileNumber = -1;
        String filetype = "";

        int highResProfileNumber = 3;
        String highResFiletype = Config.METUS_PROFILE_FILETYPES[highResProfileNumber - 1];

        for (int i = 0; i < Config.METUS_NUMBER_OF_PROFILES; i++) {
            if (Config.METUS_PROFILES[i].equals(profile)) {
                profileNumber = i + 1;
                filetype = Config.METUS_PROFILE_FILETYPES[i];
            }
        }

        if (profileNumber == -1 || filetype.equals("")) {
            CumUtils.show("Invalid profile.");
            return;
        }

        SocketConnection connection;

        try {
            connection = new SocketConnection(Config.METUS_HOST, Config.METUS_PORT);
        } catch (Exception e) {
            CumUtils.show("Error when connecting to Metus.");
            return;
        }

        String filename = item.getId();

        if (!connection.loadProject(Config.METUS_PROJECT_PATH)) {
            CumUtils.show("Couldn't load Metus project.");
            return;
        }

        if (connection.isRunning(encoder)) {
            CumUtils.show("Encoder busy.");
        } else {
            if (!connection.setFilename(encoder, filename)) {
                CumUtils.show("Couldn't set Metus filename.");
                return;
            }
            if (!(connection.startIngest(encoder, profileNumber) &&
                    connection.startIngest(encoder, highResProfileNumber))) {
                CumUtils.show("Couldn't start Metus ingest.");
                return;
            }

            item.log("Ingestion loaded with encoder " + encoder + ", profile " + profile);

            item.setIngestStatus("Ingesting");

            item.setAssetName(filename + filetype);
            item.setHighResName(filename + highResFiletype);

            item.save();
        }

        try {
            connection.close();
        } catch (Exception e) {
            CumUtils.show("Error when closing connection.");
        }
    }
}
